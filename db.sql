create schema inventory collate utf8_general_ci;
use inventory;

create table categories
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);

create table locations
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);

create table items
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    category_id int          not null,
    location_id int          not null,
    description text         null,
    image       varchar(31)  null,
    datetime    date         null,
    constraint items_categories_id_fk
        foreign key (category_id) references categories (id)
            on update cascade,
    constraint items_locations_id_fk
        foreign key (location_id) references locations (id)
            on update cascade
);



insert into inventory.categories (id, title, description)
values  (1, 'Мебель', 'Something desc.'),
        (2, 'Компьютерное оборудование', 'Something desc.'),
        (3, 'Бытовая техника', null);

insert into inventory.locations (id, title, description)
values  (1, 'Кабинет директора', 'Something description'),
        (2, 'Офис 204', null),
        (3, 'Учительская', 'qwerty');


insert into inventory.items (id, title, category_id, location_id, description, image, datetime)
values  (1, 'Ноутбук HP Probook 450', 2, 1, '', 'SenbZMNLlITwnmGjNkgsM.jpeg', '2022-02-07'),
        (2, 'Кресло компьютерное KK-345', 1, 2, 'АААААААААААААА', null, '2022-02-12'),
        (3, 'ВСТРАИВАЕМАЯ МИКРОВОЛНОВАЯ ПЕЧЬ HANSA AMMB44E1XH', 3, 3, '', 'jenXKp14KxYN1x4qKiYwr.png', '2022-02-11');
