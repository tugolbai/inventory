const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT categories.id, categories.title FROM categories';
        let [categories] = await db.getConnection()
            .execute(query);
        return res.send(categories);
    } catch (e) {
        next(e);
    }
})

router.get('/:id', async (req, res, next) => {
    try {
        const [categories] = await db.getConnection().execute('SELECT * FROM categories WHERE id = ?', [req.params.id]);

        const category = categories[0];

        if (!category) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(category);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({message: 'Title is required'});
        }

        const category = {
            title: req.body.title,
            description: null,
        };

        if (req.body.description) {
            category.description = req.body.description;
        }


        let query = 'INSERT INTO categories (title, description) VALUES (?, ?)';

        const [results] = await db.getConnection().execute(query, [
            category.title,
            category.description,
        ]);

        const id = results.insertId;

        return res.send({message: 'Created new category', id});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM inventory.categories WHERE id = ?', [req.params.id]);
        return res.send({message: 'Category with id ' + req.params.id + ' removed'});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

module.exports = router;