const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT items.id, items.title, items.category_id, items.location_id FROM items';
        let [items] = await db.getConnection()
            .execute(query);
        return res.send(items);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [items] = await db.getConnection().execute('SELECT * FROM items WHERE id = ?', [req.params.id]);

        const item = items[0];

        if (!item) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(item);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.category_id || !req.body.location_id) {
            return res.status(400).send({message: 'Title, category and  location are required'});
        }

        const item = {
            title: req.body.title,
            category_id: parseFloat(req.body.category_id),
            location_id: parseFloat(req.body.location_id),
            description: null,
            image: null,
            datetime: new Date().toISOString().substr(0, 10),
        };

        if (req.file) {
            item.image = req.file.filename;
        }
        if (req.body.description) {
            item.description = req.body.description;
        }

        let query = 'INSERT INTO items (title, category_id, location_id, description, image, datetime) VALUES (?, ?, ?, ?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            item.title,
            item.category_id,
            item.location_id,
            item.description,
            item.image,
            item.datetime,
        ]);

        const id = results.insertId;

        return res.send({message: 'Created new item', id});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM inventory.items WHERE id = ?', [req.params.id]);
        return res.send({message: 'Item with id ' + req.params.id + ' removed'});

    } catch (e) {
        next(e);
        return res.send({messages: e.message});

    }
});

module.exports = router;