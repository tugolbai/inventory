const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT locations.id, locations.title FROM locations';
        let [locations] = await db.getConnection()
            .execute(query);
        return res.send(locations);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [locations] = await db.getConnection().execute('SELECT * FROM locations WHERE id = ?', [req.params.id]);

        const location = locations[0];

        if (!location) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(location);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title) {
            return res.status(400).send({message: 'Title is required'});
        }

        const location = {
            title: req.body.title,
            description: null,
        };

        if (req.body.description) {
            location.description = req.body.description;
        }

        let query = 'INSERT INTO locations (title, description) VALUES (?, ?)';

        const [results] = await db.getConnection().execute(query, [
            location.title,
            location.description,
        ]);

        const id = results.insertId;

        return res.send({message: 'Created new location', id});
    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM inventory.locations WHERE id = ?', [req.params.id]);
        return res.send({message: 'Location with id ' + req.params.id + ' removed'});

    } catch (e) {
        next(e);
        return res.send({messages: e.message});
    }
});

module.exports = router;